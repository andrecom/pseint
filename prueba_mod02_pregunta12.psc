Algoritmo prueba_mod02_pregunta12
//	Se requiere determinar el las imposiciones de un trabajador 
//	con base en las horas que trabaja y el pago por hora que recibe.
//	Tomando en cuenta que las imposiciones de este trabajador estan conformadas
//	por un 10% destinado al pago de AFP y un 7% al pago de Salud.
//	Realice el diagrama de flujo y el pseudoc�digo que representen el algoritmo
//	de soluci�n correspondiente.
	
	Escribir "Deseas saber cuanto debes pagar en imposiciones (AFP/Salud) y cu�nto recibir�s a fin de mes?"
	Escribir "Usaremos esta herramienta"
	Escribir "Ingresa la cantidad de horas que trabajaste en el mes"
	Leer horas
	Escribir "Ahora, ingresa el valor de la hora trabajada"
	Leer valorhora
	
	vBr=(horas*valorhora)
	afp=(vBr*0.1)
	salud=(vBr*0.07)
	vLr=[(vBr-afp)-salud]
		
	Escribir "Tu sueldo bruto es de $" vBr	" pesos"
	Escribir "Considerando que las imposiciones por AFP y SAlud corresponden a 10% y 7% de tu sueldo"
	Escribir "Tu sueldo l�quido corresponde a " vLr " pesos"
	Escribir "Por lo tanto, pagar�s $" afp " en AFP y $" salud " en Salud."
	
FinAlgoritmo
