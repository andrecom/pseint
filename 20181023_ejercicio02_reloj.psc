Algoritmo reloj_
	// Hacer un diagrama de flujo que simule un reloj
	
	Definir H,M,S como numeros	
	H=23
	M=59
	S=50
	
	Mientras H<24 Hacer
		
		Mientras M<60 Hacer
						
			Mientras S<60 Hacer
				Borrar Pantalla
				Escribir H " : " M " : " S
								
				Esperar 1 segundo // 1 segundo //
				S=S+1 // contador de segundos
				
			Fin Mientras
			
			M=M+1 //contador de minutos
			S=0	
		Fin Mientras
		
		H=H+1 //contador de horas
		M=0
		
		// bucle de horaa
		Si H=24 Entonces
			H=0

		Fin Si	
		
	Fin Mientras
	
FinAlgoritmo
