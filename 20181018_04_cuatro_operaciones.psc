Algoritmo cuatro_operaciones
	// Algoritmo que lea dos numeros, calculando y escribiendo el valor su 
	// suma, resta, producto y división
	Definir valor1 Como Real
	Definir valor2 Como Real
	Escribir 'Vamos a realizar las cuatro operaciones básicas'
	Escribir 'Introduzca el primer valor'
	Leer valor1
	Escribir 'Introduzca el segundo valor'
	Leer valor2
	A = valor1+valor2
	B = valor1-valor2
	C = valor1*valor2
	D = valor1/valor2
	Escribir 'La suma de ambos es ',A
	Escribir 'La resta de ambos es ',B
	Escribir 'La multiplicación de ambos es ',C
	Escribir 'La división de ambos es ',D
FinAlgoritmo

