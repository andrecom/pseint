Algoritmo Kill_the_dragon
	Definir opcion_1 Como Caracter
	definir ansvik1 como numero
	
	Escribir "Kill the Dragon"
	Escribir "Bienvenido"
	Escribir "Has entrado a una historia en la cual ser�s un vikingo con una sola misi�n"
	Escribir "Matar al drag�n que aterroriza a tu pueblo"
	Escribir "Primero, ese vikingo debe tener un nombre. Elige uno:"	
	Leer vkgname
	Escribir "Te prometo que de esta historia no saldr�s vivo, " Mayusculas(vkgname) ". Te lo aseguro."
	Escribir "Eso, porque es probable que no puedas responder los acertijos que te voy a proponer"
	
	Escribir "�Continuas? Y/N"
	Leer continuar

	tries=0
// 	quest1=4

// PRIMERA PARTE DE ACERTIJOS
	Si  Mayusculas(continuar)="N" Entonces
		Escribir "TE PERDISTE UNA BUENA AVENTURA, " Mayusculas(vkgname)
		Escribir "Adi�s"
		
	SiNo
		Escribir "Tu primera prueba"
		Repetir
			tries=tries+1
			Escribir "�Cu�nto es 2+2?"
			Leer ansvik1
			Si ansvik1<>4 y tries<=3 Entonces
				Segun azar(3) Hacer
					0:
						Escribir "Estamos mal, prueba de nuevo"
					1:
						Escribir "Vamos, dale otra vez"
					2:
						Escribir "De verdad te cuesta?"
						
					De Otro Modo:
						Escribir "Perdiste, ya estas muerto por tonto"
				Fin Segun
				
			SiNo
				Escribir "Buena respuesta"
				next=1
				
			Fin Si	
		Hasta Que (ansvik1=4 o tries>=3)
		
		Si next=1 Entonces
			Escribir "Sigamos"
		SiNo
			Escribir "PERDISTE"
		Fin Si
				// ** Aqui debo escribir una estructura que me permita salir de la rutina.**
		Escribir "�Continuas? Y/N"
	Fin Si	
	
// SEGUNDA PARTE DE ACERTIJOS
	Leer continuar2
	
	tries=0
	quest2="gomita"
		
	//Inicio Segunda Rutina
	Si  Mayusculas(continuar2)="N" Entonces
		Escribir "IBAS BIEN :-( TE PERDISTE UNA BUENA AVENTURA, " Mayusculas(vkgname)
		Escribir "Adi�s"
			
	SiNo
		Escribir "Tu segunda prueba"
		Repetir
			tries=tries+1
			Escribir "�Qu� le pasa a un frugele cuando se cura? Es una palabra que empieza con g"
			Leer ansvik2
			Si ansvik2<>Minusculas(quest2) y tries<=3 Entonces
				Segun azar(3) Hacer
					0:
						Escribir "�Nunca has bebido acaso? Prueba de nuevo"
					1:
						Escribir "Trata de pensar l�gicamente, �vale?"
					2:
						Escribir "En serio, es muy f�cil"
							
					De Otro Modo:
						Escribir "Pucha, perdiste. Pero nunca has bebido. Bien por eso."
				Fin Segun
					
			SiNo
				Escribir "Se nota que te has curado alguna vez. Buena respuesta"
				next2=1
				
			Fin Si
			
		Hasta Que (ansvik2=Minusculas(quest2) o tries>=3)
			
		Si next2=1 Entonces
			Escribir "Vamos por buen camino para llegar al drag�n."
			goto3=1
		SiNo
			Escribir "PERDISTE PORQUE NUNCA HAS BEBIDO, PARECE."
			// ** Aqui debo escribir una estructura que me permita salir de la rutina.**
			
		Fin Si
			
		
		Si goto3=1 Entonces
			Escribir "�Continuas? Y/N"
		SiNo 
			
		FinSi
	Fin Si // Fin Segunda Rutina
		
//	TERCERA Y ULTIMA PARTE, FACE THE DRAGON
		
	Leer continuar
//	tries=0
		
	Si  Mayusculas(continuar2)="N" Entonces
		Escribir "IBAS BIEN :-( TE PERDISTE UNA BUENA AVENTURA, " Mayusculas(vkgname)
		Escribir "Adi�s"
			
	SiNo
		Escribir "Lleg� la hora de enfrentar al drag�n!!!"
		Escribir "S� inteligente y no mueras"
		Escribir "Tendr�s que lanzar el dado (entre 1 y 6)"
		Escribir "Presiona una tecla"
		Esperar Tecla
		
		numfin=Azar(5)+1
		Esperar 2 segundos
		
		Escribir "Tu n�mero del dado es " numfin "."
		
		Segun numfin Hacer
			(1):			// Armas: Espada y Escudo, mataste el drag�n
				Escribir "Encontraste una espada y un escudo. Mataste al drag�n"
			(2):			// Lo viste, te cagaste en tus mallas y el drag�n te mat�
				Escribir "Lo viste, te cagaste en tus mallas y el drag�n te mat�"
			(3):			// No viste al dragon, te fuiste pero el drag�n estaba detr�s y te comi�
				Escribir "No viste al dragon, te fuiste pero el drag�n estaba detr�s y te comi�"
			(4):			// Armas, encontraste un hacha y con esa, mataste al drag�n. 
				Escribir "Encontraste un hacha antigua y poderosa. Con esa, mataste al drag�n."
			(5):			// S�lo con una honda y una piedra que hab�a cerca, mataste al drag�n
				Escribir "Con una honda y una piedra que hab�a cerca, mataste al drag�n"
			(6):			// Opci�n que el drag�n te mat�
				Escribir "El drag�n te mat�"
				
			De Otro Modo:		//
				Escribir "Esto sali� mal. Lanza de nuevo. "
		Fin Segun
	Fin Si

FinAlgoritmo

// respuestas posibles

//	Escribir "Buena respuesta"
//	Escribir "�Continuas? Y/N"
