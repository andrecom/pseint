Algoritmo ejercicio_02
	// modificar el digrama anterior pero para
	// sumar 100 numeros leidos por teclado.
	cont=0
	acum=0
	Mientras cont<100 Hacer //despu�s de mientras "c<100", es la condicion que siempre se va cumplir, como un bucle, un loop,
							// hasta que la condici�n deje de cumplirse. En el momento en que cont no sea menor que 100, el bucle
							// se detiene.	
		Escribir "escriba un numero"
		leer valor_usuario
		
		//contador
		cont=cont+1
		
		//Acumulador
		acum=acum+valor_usuario
		
		Escribir "yo soy el acumulador " acum 
		Escribir "yo soy el contador " cont
				
		
	Fin Mientras
	
FinAlgoritmo

// El acumulador y el contador se utilizan en estrutucturas repetitivas.
