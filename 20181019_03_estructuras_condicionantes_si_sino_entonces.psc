	Algoritmo condicionantes_y_operadores_logicos
		// Algoritmo que lea tres numerpos distintos y ns diga cual de ellos
		// es el mayor.
		// Recuerda usar la estructura condicional Si y operadores logicos
		//
		
		Escribir "Introduzca el valor 1"
		Leer valor1
		
		Escribir "Introduzca el valor 2"
		Leer valor2
		
		Escribir "Introduzca el valor 3"
		Leer valor3
		
		Si valor1>valor2 y valor1>valor3 Entonces
			escribir "el valor 1 es mayor"
		SiNo
			si valor2>valor3 y valor2>valor1 Entonces
				Escribir "El valor de 2 es mayor"
			SiNo
				si valor3>valor1 y valor3>valor2 Entonces
					Escribir "El valor de 3 es mayor"
				SiNo
					Escribir "Los valores son iguales"
				FinSi
			FinSi
		FinSi
		
FinAlgoritmo
