Algoritmo fibonacci
	// la sucesi�n Fibonacci  se define de la siguiente forma a(1)=1, a(2)=1, y a(n)=a(n-1)+a(n-2) para n>2, es decir
	// los dos primeros son 1 y el resto cada uno es la suma de los dos anteriores. Los primeros n�meros de la 
	// sucesi�n son 1, 1, 2, 3, 5, 8, 13, 21...
	// hacer un diagrama de flujo para calcular el n-esimo t�rmino de la sucesi�n
	
	cont=0
	Leer n // veces que se repetir� la iteraci�n
	// variables
	a=0
	b=1
	
	
	Para a<-1 Hasta n
		c=a+b
		Escribir c 
	Fin Para
	
	
FinAlgoritmo
